SELECT customerName FROM customers WHERE country = "Philippines"; 

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT lastName, firstName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%dhl%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country from customers;

SELECT DISTINCT status from orders;

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

SELECT firstName, lastName FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employeeNumber = "1166";

SELECT productName, customerName FROM products
	JOIN orderdetails ON products.productCode = orderdetails.productCode 
	JOIN orders ON orderdetails.orderNumber = orders.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customerName = "Baane Mini Imports";

SELECT firstName, lastName, customerName, offices.country FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "%+81%";
